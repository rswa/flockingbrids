﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMover : MonoBehaviour {
    public Transform targetTransform;
    public float speed = 10f;
    public float delay = 3f;
    [SerializeField]
    Transform[] points;
	// Use this for initialization
	IEnumerator Start () {
        var index = 0;
        while(true) {
            yield return null;

            var distance = Vector3.Distance(targetTransform.position, points[index].position);
            var moveDistance = speed * Time.deltaTime;
            if (moveDistance >= distance) {
                targetTransform.position = points[index++].position;
                if(index >= points.Length) index = 0;
                yield return new WaitForSeconds(delay);
            }
            else {
                targetTransform.position = Vector3.Lerp(targetTransform.position, points[index].position, moveDistance / distance);
            }
        }
	}
}
