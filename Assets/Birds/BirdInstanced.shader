﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Instanced/BirdInstanced" {
    Properties{
        _MainTex("Albedo (RGB)", 2D) = "white" {}
    }

    SubShader{
        Tags{ "RenderType" = "Opaque" }

        Pass{

            Tags{ "LightMode" = "ForwardBase" }
            Cull Off
        ZWrite On

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
            #pragma target 4.5
            
            #include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"
            #include "AutoLight.cginc"

            sampler2D _MainTex;

            #if SHADER_TARGET >= 45
            StructuredBuffer<float4> positionBuffer;
            StructuredBuffer<float4> velocityBuffer;
            #endif

            struct VS_INPUT {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord : TEXCOORD0;
                float4 color : COLOR0;
                uint vertexId   : SV_VertexID;
            };

            struct v2f {
                float4 pos : SV_POSITION;
                float2 uv_MainTex : TEXCOORD0;
                float3 ambient : TEXCOORD1;
                float3 diffuse : TEXCOORD2;
                float4 color : TEXCOORD3;
                SHADOW_COORDS(4)
            };

            v2f vert(VS_INPUT v, uint instanceID : SV_InstanceID) {
                #if SHADER_TARGET >= 45
                float4 tmpPos = positionBuffer[instanceID];
                float3 direction = normalize(velocityBuffer[instanceID].xyz);
                #else
                float4 tmpPos = 0.;
                float3 direction = float3(0., 0., 1.);
                #endif

 

                float3 newPosition = v.vertex.xyz;
                if (v.vertexId == 4 || v.vertexId == 7) {
                    // flap wings
                    newPosition.y = sin(tmpPos.w) * 0.3;
                }
                //newPosition = mul(unity_ObjectToWorld, newPosition);

                float xz = length(direction.xz);
                float cosry = direction.z / xz;
                float sinry = direction.x / xz;

                float cosrz = sqrt(1. - direction.y * direction.y);
                float sinrz = direction.y;

                float3x3 maty = float3x3(
                    cosry, 0, sinry,
                    0, 1, 0,
                    -sinry, 0, cosry
                );

                float3x3 matz = float3x3(
                    cosrz, -sinrz, 0,
                    sinrz, cosrz, 0,
                    0, 0, 1
                );
                
                float3 worldNormal = mul(maty, mul(matz, v.normal));
                newPosition = mul(maty, mul(matz, newPosition));
                newPosition = newPosition + tmpPos.xyz;

                float3 ndotl = saturate(dot(worldNormal, _WorldSpaceLightPos0.xyz));
                
                v2f o;
                o.uv_MainTex = v.texcoord;
                o.ambient = ShadeSH9(float4(worldNormal, 1.0f));
                o.diffuse = (ndotl * _LightColor0.rgb);;
                o.color = v.color;
                o.pos = mul(UNITY_MATRIX_VP, float4(newPosition, 1.0f));
                TRANSFER_SHADOW(o)
                return o;
            }

            fixed4 frag(v2f i) : SV_Target {
                fixed shadow = SHADOW_ATTENUATION(i);
                fixed4 albedo = tex2D(_MainTex, i.uv_MainTex);
                float3 lighting = i.diffuse * shadow + i.ambient;
        
                //float z = 0.2 + (1000. - i.color.w) / 1000.;
                //fixed4 output = fixed4(albedo.rgb * i.color * lighting * z, albedo.w);
                fixed4 output = fixed4(albedo.rgb * i.color * lighting, albedo.w);

                UNITY_APPLY_FOG(i.fogCoord, output);
                return output;
            }

            ENDCG
        }

        Pass{
            Name "ShadowCaster"
            Tags{ "LightMode" = "ShadowCaster" }
            Cull Off
            ZWrite On
            ZTest LEqual

            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_shadowcaster
            #pragma target 4.5

            #include "UnityCG.cginc"

            sampler2D _MainTex;

            #if SHADER_TARGET >= 45
            StructuredBuffer<float4> positionBuffer;
            StructuredBuffer<float4> velocityBuffer;
            #endif

            struct VS_INPUT {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord : TEXCOORD0;
                uint vertexId   : SV_VertexID;
            };

            struct v2f {
                V2F_SHADOW_CASTER;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            v2f vert(VS_INPUT v, uint instanceID : SV_InstanceID) {

                #if SHADER_TARGET >= 45
                float4 tmpPos = positionBuffer[instanceID];
                float3 direction = normalize(velocityBuffer[instanceID].xyz);
                #else
                float4 tmpPos = 0.;
                float3 direction = float3(1., 0., 0.);
                #endif

                float3 newPosition = v.vertex.xyz;

                if (v.vertexId == 4 || v.vertexId == 7) {
                    // flap wings
                    newPosition.y = sin(tmpPos.w) * 0.3;
                }

                direction.z *= -1.;
                float xz = length(direction.xz);
                float xyz = 1.;
                float x = sqrt(1. - direction.y * direction.y);
                float cosry = direction.x / xz;
                float sinry = direction.z / xz;
                float cosrz = x / xyz;
                float sinrz = direction.y / xyz;

                newPosition = mul(unity_ObjectToWorld, newPosition);

                float3x3 maty = float3x3(
                    cosry, 0, -sinry,
                    0, 1, 0,
                    sinry, 0, cosry
                    );

                float3x3 matz = float3x3(
                    cosrz, sinrz, 0,
                    -sinrz, cosrz, 0,
                    0, 0, 1
                    );

                float3x3 mat = mul(maty, matz);

                newPosition = mul(mat, newPosition);
                newPosition = newPosition + tmpPos.xyz;

                v2f o;
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                #ifdef SHADOWS_CUBE
                o.vec = newPosition - _LightPositionRange.xyz;
                o.pos = mul(UNITY_MATRIX_VP, float4(newPosition, 1.0f));
                #else
                
                if (unity_LightShadowBias.z != 0.0) {
                    float3 worldNormal = mul(mat, v.normal);
                    float3 wLight = normalize(UnityWorldSpaceLightDir(newPosition.xyz));

                    // apply normal offset bias (inset position along the normal)
                    // bias needs to be scaled by sine between normal and light direction
                    // (http://the-witness.net/news/2013/09/shadow-mapping-summary-part-1/)
                    //
                    // unity_LightShadowBias.z contains user-specified normal offset amount
                    // scaled by world space texel size.

                    float shadowCos = dot(worldNormal, wLight);
                    float shadowSine = sqrt(1 - shadowCos*shadowCos);
                    float normalBias = unity_LightShadowBias.z * shadowSine;

                    newPosition.xyz -= worldNormal * normalBias;
                }
                o.pos = mul(UNITY_MATRIX_VP, float4(newPosition, 1.0f));
                o.pos = UnityApplyLinearShadowBias(o.pos);
                #endif
                
                return o;
            }

            fixed4 frag(v2f i) : SV_Target{
                SHADOW_CASTER_FRAGMENT(i)
            }

            ENDCG
        }
    }
}