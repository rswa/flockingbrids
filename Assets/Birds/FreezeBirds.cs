﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeBirds : MonoBehaviour {

    public int instanceCount = 1024;
    public float spawnRadius = 10f;
    public float spawnVelocity = 0.5f;
    protected ComputeBuffer positionBuffer;
    protected ComputeBuffer velocityBuffer;

    int cachedInstanceCount;

    protected Vector4[] tmpBuffer;
    // Use this for initialization
    protected virtual void Start() {}

    protected virtual void UpdateBuffers() {
        if(positionBuffer != null) positionBuffer.Release();
        if(velocityBuffer != null) velocityBuffer.Release();

        positionBuffer = new ComputeBuffer(instanceCount, 16);
        velocityBuffer = new ComputeBuffer(instanceCount, 16);

        tmpBuffer = new Vector4[instanceCount];
        for(int i = 0; i < instanceCount; i++) {
            var position = Random.insideUnitSphere * spawnRadius;
            tmpBuffer[i] = new Vector4(position.x, position.y, position.y, 1);
        }
        positionBuffer.SetData(tmpBuffer);

        for(int i = 0; i < instanceCount; i++) {
            var velocity = Random.insideUnitSphere * spawnVelocity;
            tmpBuffer[i] = new Vector4(velocity.x, velocity.y, velocity.z, 1);
        }

        velocityBuffer.SetData(tmpBuffer);

        cachedInstanceCount = instanceCount;
    }

    protected virtual void Update() {
        if(cachedInstanceCount != instanceCount) UpdateBuffers();
    }

    protected virtual void OnDestroy() {
        if(positionBuffer != null) positionBuffer.Release();
        positionBuffer = null;
        if(velocityBuffer != null) velocityBuffer.Release();
        velocityBuffer = null;
    }
}
