﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdsInstanced : FlockingBirds {


    public UnityEngine.Rendering.ShadowCastingMode shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
    [SerializeField]
    Material instanceMaterial;

    Mesh instanceMesh;
    ComputeBuffer argsBuffer;

    private uint[] args = new uint[5] { 0, 0, 0, 0, 0 };
    
    // Use this for initialization
    protected override void Start () {
        base.Start();
        argsBuffer = new ComputeBuffer(1, args.Length * sizeof(uint), ComputeBufferType.IndirectArguments);
        CreateBirdMesh();
        UpdateBuffers();
    }

    void CreateBirdMesh() {
        if(instanceMesh != null) Destroy(instanceMesh);

        instanceMesh = new Mesh();

        var wingSemiSpanLength = 0.25f;
        var wingRootChordLengh = 0.2f;
        var backBodyLength = 0.5f;
        var frontbodyLength = 0.4f;
        var bodyHeight = 0.05f;
        instanceMesh.vertices = new Vector3[] {
            // Body
            new Vector3(0, 0, -backBodyLength), new Vector3(0, bodyHeight, -backBodyLength), new Vector3(0, 0, frontbodyLength),
            // Left Wing
            new Vector3(0, 0, -wingRootChordLengh), new Vector3(-wingSemiSpanLength, 0, 0), new Vector3(0, 0, wingRootChordLengh),
            // Right Wing
            new Vector3(0, 0, wingRootChordLengh), new Vector3(wingSemiSpanLength, 0, 0), new Vector3(0, 0, -wingRootChordLengh),
        };

        var bodyColor = new Color(0.3f, 0.3f, 0.3f);
        var wingColor = new Color(0.6f, 0.6f, 0.6f);
        instanceMesh.colors = new Color[] {
            bodyColor, bodyColor, bodyColor,
            wingColor, wingColor, wingColor,
            wingColor, wingColor, wingColor,
        };

        instanceMesh.uv = new Vector2[] {
            new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0),
            new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0),
            new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0),
        };

        instanceMesh.triangles = new int[] {
            0, 1, 2,
            3, 4, 5,
            6, 7, 8
        };

        instanceMesh.RecalculateNormals();
        instanceMesh.RecalculateBounds();
    }

    public static readonly Bounds InstancedBounds = new Bounds(Vector3.zero, Vector3.one * 40f);
    protected override void UpdateBuffers() {
        base.UpdateBuffers();
        
        instanceMaterial.SetBuffer("positionBuffer", positionBuffer);
        instanceMaterial.SetBuffer("velocityBuffer", velocityBuffer);

        // indirect args
        args[0] = instanceMesh.GetIndexCount(0);
        args[1] = (uint)instanceCount;
        argsBuffer.SetData(args);
    }

    protected override void OnDestroy() {
        base.OnDestroy();

        if (instanceMesh != null) Destroy(instanceMesh);
        if(argsBuffer != null) argsBuffer.Release();
        argsBuffer = null;
    }

    // Update is called once per frame
    protected override void Update() {
        base.Update();

        Graphics.DrawMeshInstancedIndirect(instanceMesh, 0, instanceMaterial, InstancedBounds, argsBuffer, 0, null, shadowCastingMode);
    }
}
