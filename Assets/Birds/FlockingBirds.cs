﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockingBirds : FreezeBirds {
    public ComputeShader birdShader;
    public bool flockingUpdate = true;
    public Transform targetTransform;
    [Range(1, 125)]
    public float targetFactor = 5;
    public Transform predatorTransform;
    [Range(1, 500)]
    public float predatorFactor = 5f;
    [Range(1, 100)]
    public float predatorRadius = 10f;
    [Range(1, 50)]
    public float predatorExtraLimitSpeed = 5f;

    [Range(0, 50)]
    public float seperationDistance = 0.3f;
    [Range(0, 50)]
    public float alignmentDistance = 1f;
    [Range(0, 50)]
    public float cohesionDistance = 2f;

    [Range(-1, 1)]
    public float seperationSpeedFactor = 0.06f;
    [Range(-1, 1)]
    public float alignmentSpeedFactor = 0.06f;
    [Range(-1, 1)]
    public float cohesionSpeedFactor = 0.06f;

    [Range(1, 50)]
    public float limitSpeed = 10f;
    int positionKernalIndex;
    int velocityKernalIndex;
    int threadGroupsX;

    protected override void Start() {

        positionKernalIndex = birdShader.FindKernel("CSPositionMain");
        velocityKernalIndex = birdShader.FindKernel("CSVelocityMain");

        uint threadGroupSizesX;
        uint threadGroupSizesY;
        uint threadGroupSizesZ;
        birdShader.GetKernelThreadGroupSizes(positionKernalIndex, out threadGroupSizesX, out threadGroupSizesY, out threadGroupSizesZ);
        threadGroupsX = instanceCount / (int)threadGroupSizesX;

        base.Start();
    }

    protected override void UpdateBuffers() {
        base.UpdateBuffers();

        birdShader.SetBuffer(positionKernalIndex, "positionBuffer", positionBuffer);
        birdShader.SetBuffer(positionKernalIndex, "velocityBuffer", velocityBuffer);

        birdShader.SetBuffer(velocityKernalIndex, "positionBuffer", positionBuffer);
        birdShader.SetBuffer(velocityKernalIndex, "velocityBuffer", velocityBuffer);
    }

    protected override void Update() {
        base.Update();

        if(flockingUpdate == false) return;

        birdShader.SetFloat("deltaTime", Time.deltaTime);

        var predator = Vector3.zero;
        if (predatorTransform != null) {
            predator = predatorTransform.position;
        }
         
        birdShader.SetFloats("predator", predator.x, predator.y, predator.z);
        birdShader.SetFloat("predatorFactor", predatorFactor);
        birdShader.SetFloat("predatorRadius", predatorRadius);
        birdShader.SetFloat("predatorExtraLimitSpeed", predatorExtraLimitSpeed);
        
        birdShader.SetFloat("seperationDistance", seperationDistance);
        birdShader.SetFloat("alignmentDistance", alignmentDistance);
        birdShader.SetFloat("cohesionDistance", cohesionDistance);
        birdShader.SetFloat("seperationSpeedFactor", seperationSpeedFactor);
        birdShader.SetFloat("alignmentSpeedFactor", alignmentSpeedFactor);
        birdShader.SetFloat("cohesionSpeedFactor", cohesionSpeedFactor);
        birdShader.SetFloat("limitSpeed", limitSpeed);
        
        var target = Vector3.zero;
        if (targetTransform != null) {
            target = targetTransform.position;
        }
        birdShader.SetFloats("target", target.x, target.y, target.z);
        birdShader.SetFloat("targetFactor", targetFactor);

        birdShader.Dispatch(velocityKernalIndex, threadGroupsX, 1, 1);
        birdShader.Dispatch(positionKernalIndex, threadGroupsX, 1, 1);
        //velocityBuffer.GetData(tmpBuffer);
        //Debug.LogFormat("Velocity = {0}", tmpBuffer[0]);
    }

    protected override void OnDestroy() {
        base.OnDestroy();
    }
}
