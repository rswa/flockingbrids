# README #
* this project is ported from [birds of three.js](https://threejs.org/examples/#webgl_gpgpu_birds) to Unity 5.6.0f3.
* use compute shader and GPU instancing to make flocking birds.
* red sphere is a draggable predator .
* green sphere is target of birds.